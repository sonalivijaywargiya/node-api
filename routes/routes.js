'use stricts'

module.exports=function(app){
    var user=require('../controller/userController.js');

    app.route('/createUser').post(user.create_a_user);
    app.route('/getUser').get(user.read_a_user);
}