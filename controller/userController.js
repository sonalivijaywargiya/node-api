var userdetails=require('../model/userModel.js');

exports.create_a_user=function(req,res){
    //console.log(req);
    var new_user=new userdetails(req.body);
    userdetails.createUserDetails(new_user, function(err,userres){
        if(err){
            res.send(err);
        }
        else{
            res.json(userres);
        }
    })
}
exports.read_a_user=function(req,res){
 
    userdetails.getUserDetails(req.params, function(err,userData){
        if(err){
            res.send(err);
        }
        else{
            res.json(userData);
        }
    })
}